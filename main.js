// TODO / BSG:
// - mode triche ?

// Test permettant de valider que le nombre est entre les bornes attendues
let testBornes1NoMax = {
    expects: [ 'mininum' ],
    message: 'Vous devez saisir un nombre supérieur ou égal à {mininum}',
    validate: function(value, pars) {
      return (parseInt(value) >= parseInt(pars.mininum));
    }
};
approve.addTest(testBornes1NoMax, 'testBornes1NoMax');

let controlInputs = {
  //défini les règles du champ nbrMaxToFind
  maxToFind: {
    required: { message: 'Veuillez entrer un nombre.' },
    numeric: { message: 'Veuillez n\'entrer que des chiffres.' },
    decimal: { decimal: false, message: 'Entrez un nombre entier.' },
    testBornes1NoMax: { mininum: '2' }
  },
  //défini les règles du champ nbrTry
  try: {
    required: { message: 'Veuillez entrer un nombre.' },
    numeric: { message: 'Veuillez n\'entrer que des chiffres.' },
    decimal: { decimal: false, message: 'Entrez un nombre entier.' },
    testBornes1NoMax: { mininum: '1' }
  },
  //défini les règles du champ enterNumber
  enterNumber: {
    required: { message: 'Veuillez entrer un nombre.' },
    numeric: { message: 'Veuillez n\'entrer que des chiffres.' },
    decimal: { decimal: false, message: 'Entrez un nombre entier.' },
    testBornes1NoMax: { mininum: '1'}
  }
};

let nbrMaxToFind = 100;
let nbrComputer = -1;
let nbrTryUsed = 1;
let tryLeft = ($('#nbrTry').val() - nbrTryUsed);

/*let nbrMaxToFind = number().positive().integrer();*/
// met des écouteurs sur les boutons et definis des des valeurs
function init(){
  $('#play').on('click', play);
  $('#nbrMaxToFind').val(nbrMaxToFind);
  $('#nbrTry').val(nbrMaxToFind / 20);
  $('#nbrMaxToFind').on('change', computeNbrTry);
  $('#enterNumber, #stop, #validNumber').prop('disabled', true);
  $('#validNumber').on('click', checkNumber);
  $('#stop').on('click', stop);
  $('#enterNumber').val('');
  $('.victoryOverlay').hide();
  $('#newGame').on('click', newGame);
  $('.loseOverlay').hide();
  $('#newGameLoseOverlay').on('click', newGameLoseOverlay);
};

// calcule le nombre à trouver
function computeNumberToGuess() {
  let number;

  // D'abord on choit un nombre au hasard entre 0 et 1.
  number = Math.random();
  // Ensuite on multiplie par le maximum a trouver pour
  // obtenir un nombre entre 0 et nbrMaxToFind (/!\ float)
  number *= nbrMaxToFind;
  // on convertit en entier
  number = Math.round(number);
  // Nouveau probleme: on risque de depasser du maximum donc on doit limiter.
  number = nbrMaxToFind - (number % nbrMaxToFind);
  return number;
};
// controle que les champs sont correctement
// remplis puis lance le jeu.
// active ou desactive les champs
//remet a zero le nbr d'essais utilisé
function play() {
  nbrComputer = computeNumberToGuess();
  $('#nbrMaxToFind, #nbrTry').prop('disabled', true);
  $('#enterNumber, #stop, #validNumber').prop('disabled', false);
  $('#play').prop('disabled', true);
  //enleve les messages d'erreurs precedents
  $('#errorsNbrMaxToFind').find('label').remove();
  // controle les champs
  let resultNbrMaxToFind = approve.value($('#nbrMaxToFind').val(), controlInputs.maxToFind);
  let resultNbrTry = approve.value($('#nbrTry').val(), controlInputs.try);
  //recupere les erreurs du champ #nbrMaxToFind et les met dans un <label>
  let htmlErrorsNbrMax = '';
  for (let posErrNbrMaxToFind = 0; resultNbrMaxToFind.errors.length > posErrNbrMaxToFind; posErrNbrMaxToFind++) {
    htmlErrorsNbrMax += '<label>' + resultNbrMaxToFind.errors[posErrNbrMaxToFind] + '</label>';
  };
  //recuper les erreurs du champ #nbrTry et les mets dans un <label>
  let htmlErrorsTry = '';
  for (let posErrNbrTry = 0; resultNbrTry.errors.length > posErrNbrTry; posErrNbrTry++){
    htmlErrorsTry += '<label>' + resultNbrTry.errors[posErrNbrTry] + '</label>';
  };
  //affiche les erreurs
  $('#errorsNbrMaxToFind').html(htmlErrorsNbrMax);
  $('#errorsNbrTry').html(htmlErrorsTry);
  //controle si ily a des erreurs et desactive les champs de jeu
  if (htmlErrorsNbrMax != ''){
    $('#enterNumber, #stop, #validNumber').prop('disabled', true);
    $('#play').prop('disabled', false);
    $('#nbrMaxToFind, #nbrTry').prop('disabled', false);
  };
  //controle si ily a des erreurs et desactive les champs de jeu
  if (htmlErrorsTry != ''){
    $('#enterNumber, #stop, #validNumber').prop('disabled', true);
    $('#play').prop('disabled', false);
    $('#nbrMaxToFind, #nbrTry').prop('disabled', false);
  };
};
//calcule le nombre d'essai en fonction de nbrMaxToFind
function computeNbrTry() {
  nbrMaxToFind = parseInt($('#nbrMaxToFind').val());
  $('#nbrTry').val(Math.round(nbrMaxToFind / 20));
};
//regarde si le nombre entré est +grand ou +pettit ou égal que le nombre de l'ordi
function checkNumber() {
  let tryLeft = ($('#nbrTry').val() - nbrTryUsed);
  let nbrTry = $('#nbrTry').val();
  let nbrEntered = $('#enterNumber').val();
  let resultEnterNumber = approve.value($('#enterNumber').val(), controlInputs.enterNumber);
  let htmlEnterNumber = '';
  for (let posErrEnterNumber = 0; resultEnterNumber.errors.length > posErrEnterNumber; posErrEnterNumber++){
    htmlEnterNumber += '<label>' + resultEnterNumber.errors[posErrEnterNumber] + '</label>';
  };
  $('.errorsEnterNumber').html(htmlEnterNumber);
  if (resultEnterNumber.approved) {
    if (nbrEntered < nbrComputer){
      showMessage('Plus grand');
      nbrTryUsed += 1;
    } else if (nbrEntered > nbrComputer){
      showMessage('Plus petit');
      nbrTryUsed += 1;
    } else {
      showMessage('Gagné');
      win();
    };
    if (nbrTryUsed > nbrTry){
      lose();
    };
    $('#tryLeft').html(tryLeft);
  }
  console.log(nbrTryUsed);
};
//affiche un message: +petit ou +grand
function showMessage(message){
  $('#smallerOrBigger').html('<label class="msgSmallerOrBigger">' + message + '</label>');
  setTimeout(function clearMessages(){
    $('#smallerOrBigger').find('label[class="msgSmallerOrBigger"]').remove();
  }, 3000);

};
//arrete le jeu en desactivant / activant les champs
function stop(){
  $('#enterNumber, #validNumber, #stop').prop('disabled', true);
  $('#nbrMaxToFind, #nbrTry, #play').prop('disabled', false);
};
//selon le nombre d'essais affiche un message de victoire different
//desactive et active les champs afin que la partie s'arrete et que ne puissse pas saisir de nombre
function win(){
  $('#enterNumber, #stop, #validNumber').prop('disabled', true);
  $('#nbrMaxToFind, #nbrTry, #play').prop('disabled', false);
  showVictoryMessage();
  $('.victoryOverlay').show();
};
//calcule le message de victoiire en finctoon du nombre d'essais utilisé
function computeVictoryMessage() {
  return '';
};
//affiche les messages de voctoire
function showVictoryMessage() {
  let htmlVictoryResult = '';
  htmlVictoryResult += 'Vous avez gagné en ' + nbrTryUsed + ' essais sur ' + $('#nbrTry').val() + '<br />';
  htmlVictoryResult += computeVictoryMessage();
  $('#victoryResult').html(htmlVictoryResult);
};
//cache le menu de victoire
function newGame() {
  $('.victoryOverlay').hide();
  $('#enterNumber').val('');
  nbrTryUsed = 1;
  tryLeft = ($('#nbrTry').val() - nbrTryUsed);
};
//cache le menu de défaite
function newGameLoseOverlay(){
  $('.loseOverlay').hide();
  $('#enterNumber').val('');
  nbrTryUsed = 1;
  tryLeft = ($('#nbrTry').val() - nbrTryUsed);
};
//desactive / active les champs et montre le menu de victoire
function lose(){
  $('#enterNumber, #stop, #validNumber').prop('disabled', true);
  $('#nbrMaxToFind, #nbrTry, #play').prop('disabled', false);
  $('.loseOverlay').show();
  $('.loseOverlayNbrComputer').html('Le nombre à trouver était ' + nbrComputer + '.');
}
$(document).ready(init);
